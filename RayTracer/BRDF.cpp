#include "stdafx.h"
#include "Constantes.h"
#include "BRDF.h"

// ---------------------------------------------------------- default constructor

BRDF::BRDF(void) {}


BRDF::BRDF(const BRDF& brdf) {}



// --------------------------------------------------------------- assignment operator

BRDF&
BRDF::operator= (const BRDF& rhs) {
	if (this == &rhs)
		return (*this);

	return (*this);
}


// ---------------------------------------------------------- destructor

BRDF::~BRDF(void) {}


// ------------------------------------------------------------------------ f

ColorRGB
BRDF::f(const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi) const {
	return (negro);
}


// ------------------------------------------------------------------------ rho	

ColorRGB
BRDF::rho(const ShadeRec& sr, const Vector3D& wo) const {
	return (negro);
}

