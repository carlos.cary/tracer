#pragma once
#include "ObjetoGeometrico.h"
#include "Punto3D.h"
class Triangulo : public ObjetoGeometrico
{
public:
	Triangulo();
	Triangulo(const Punto3D& a, const Punto3D& b, const Punto3D& c );
	~Triangulo();
	bool impacto(const Rayo& r, double& t, ShadeRec& s)const;

	Punto3D v0, v1, v2;
	Vector3D normal;
};


