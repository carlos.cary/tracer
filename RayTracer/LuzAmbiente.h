#pragma once
#include "Luz.h"
class LuzAmbiente:public Luz
{
public:
	LuzAmbiente();
	~LuzAmbiente();
	

	void establecerColor(const float c);
	void establecerColor(const ColorRGB& c );
	void establecerColor(const float r, const float g, const float b);
	// Retorna la direccion de la luz
	virtual Vector3D obtenerDireccion(ShadeRec& sr);
	
	// Retorna el brillo incidente en el punto
	virtual ColorRGB L(ShadeRec& sr);

private:
	double ls;
	ColorRGB color;
};

