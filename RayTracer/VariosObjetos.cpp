#include "stdafx.h"
#include "VariosObjetos.h"
#include "Mundo.h"
#include <algorithm> 
#include <iostream>
using std::cout;
VariosObjetos::VariosObjetos()
	:Tracer()
{
}
VariosObjetos::VariosObjetos(Mundo* _worldPtr)
	: Tracer(_worldPtr)
{}


VariosObjetos::~VariosObjetos()
{
}

ColorRGB VariosObjetos::trace_ray(const Rayo& rayo)const
{

	ShadeRec sr(world_ptr->impactar_objetos(rayo));
	ColorRGB colorLuz(1.0, 0.4, 0.1);
	if (sr.impactaUnObjeto)
	{
		
		sr.color.r = 0.72 * colorLuz.r * std::max(0.0, sr.normal*sr.m.luces[0]->obtenerDireccion(sr));
		sr.color.g = 0.72 * colorLuz.g * std::max(0.0, sr.normal*sr.m.luces[0]->obtenerDireccion(sr));
		sr.color.b = 0.72 * colorLuz.b * std::max(0.0, sr.normal*sr.m.luces[0]->obtenerDireccion(sr));
		/*sr.color.r = 0.314 * colorLuz.r * std::max(0.0, n*l);
		sr.color.g = 0.314 * colorLuz.g * std::max(0.0, n*l);
		sr.color.b = 0.314 * colorLuz.b * std::max(0.0, n*l);*/
		//n.Mostrar();
		return (sr.color);
	}
	else
	{
		return world_ptr->colorFondo;
	}
}