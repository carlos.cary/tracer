#include "stdafx.h"
#include "LuzAmbiente.h"
LuzAmbiente::LuzAmbiente()
	:Luz(),
	ls(1.0),
	color(1.0) // Blanco
{
}

LuzAmbiente::~LuzAmbiente()
{
}

void LuzAmbiente::establecerColor(const float c)
{
	color.r = c;
	color.g = c;
	color.b = c;
}

void LuzAmbiente::establecerColor(const ColorRGB& c)
{
	color = c;
}

void LuzAmbiente::establecerColor(const float r, const float g, const float b)
{
	color.r = r;
	color.g = g;
	color.b = b;
}

// se retorna el vector cero, porque no se llama en ningun lado
Vector3D LuzAmbiente::obtenerDireccion(ShadeRec& sr)
{
	return(Vector3D(0.0));
}

ColorRGB LuzAmbiente::L(ShadeRec& sr)
{
	return (color * ls);
}

