#pragma once
#include "ColorRGB.h"
#include "Vector3D.h"
#include "ShadeRec.h"
class BRDF {
public:

	BRDF(void);

	BRDF(const BRDF& object);

	virtual BRDF*
		clone(void) const = 0;

	~BRDF(void);

	// la funcion f retorna el BRD mismo
	virtual ColorRGB f(const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi) const;

	// la funcion rho retorna la reflectancia bihemisférica (RHO hh)
	virtual ColorRGB rho(const ShadeRec& sr, const Vector3D& wo) const;


protected:

	BRDF &
		operator= (const BRDF& rhs);
};

