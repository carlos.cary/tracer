#include "stdafx.h"
#include "Constantes.h"
#include "ShadeRec.h"

ShadeRec::ShadeRec(Mundo& _m)
	:impactaUnObjeto(false),
	puntoImpactoLocal(),
	normal(),
	color(negro),
	m(_m),
	t(0.0)
{}
ShadeRec::ShadeRec(const ShadeRec& sr)
	:impactaUnObjeto(sr.impactaUnObjeto),
	puntoImpactoLocal(sr.puntoImpactoLocal),
	normal(sr.normal),
	color(sr.color),
	m(sr.m),
	t(sr.t)
{}
ShadeRec::~ShadeRec(){}



